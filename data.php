<?php 
$topBadgeTitle = "Gratis ebook 9 Cara Cerdas Menggunakan Domain [x]";
$logo = "assets/logo.png";
$mainTitle = "PHP Hosting";
$mainDescription = "Cepat, handal, penuh dengan modul PHP yang anda butuhkan.";
$listMenu = array("Hosting","Domain","Server","Website","Afiliasi","Promo","Pembayaran","Review","Kontak","Blog");
$flyLeftMenu = array(
    array("imageLogo"  => "assets/zenguard.png", "wording" => "PHP Zen Guard Loader"),
    array("imageLogo"  => "assets/composer.png", "wording" => "PHP Composer"),
    array("imageLogo"  => "assets/ioncube.png", "wording" => "PHP ionCube Loader")
);
$listHeaderMenu = array("Solusi PHP untuk performa query yang lebih cepat.","Konsumsi memori yang lebih rendah.","Support PHP 5.3. PHP 5.4. PHP 5.5. PHP 5.6. PHP 7","Fitur enkripsi IonCube dan Zend Guard Loaders");
$wideTitle = "Paket Hosting Singapura yang Tepat";
$wideSubTitle = "Diskon 40% + Domain dan SSL Gratis untuk Anda";
$priceMenus = array(
    array("title"  => "Bayi", "is_bestseller" => false, "real_price"=> "Rp. 19.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>14</strong></h2>.900/bln</span>", "user_count"=> 1234, 
        "features" => array("<strong>0.5x RESOURCE POWER</strong>", "500 MB Disk Space","Unlimited Bandwidth","Unlimited Databases", "1 Domain", "Instant Backup", "Unlimited SSL Gratis Selamanya")),
    array("title"  => "Pelajar", "is_bestseller" => false, "real_price"=> "Rp. 46.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>23</strong></h2>.450/bln</span>", "user_count"=> 1234, 
        "features" => array("<strong>1x Resources Power</strong>", "Unlimited Disk Space","Unlimited Bandwidth","Unlimited Pop3 Email","Unlimited Databases", "10 Addon Domains","Instant Backup","Domain Gratis Selamanya","Unlimited SSL Gratis Selamanya")),
    array("title"  => "Personal", "is_bestseller" => true, "real_price"=> "Rp. 58.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>38</strong></h2>.900/bln</span>", "user_count"=> 1234, 
    "features" => array("<strong>1x Resources Power</strong>", "Unlimited Disk Space","Unlimited Bandwidth","Unlimited Pop3 Email","Unlimited Databases", "Unlimited Addon Domains","Instant Backup","Domain Gratis Selamanya","Unlimited SSL Gratis Selamanya", "Private Name Server", "SpamAssasin Mail Protector")),
    array("title"  => "Bisnis", "is_bestseller" => false, "real_price"=> "Rp. 109.900", "disc_price" => "<span style='display:inline'>Rp. <h2 style='display:inline'><strong>65</strong></h2>.900/bln</span>", "user_count"=> 1234, 
    "features" => array("<strong>1x Resources Power</strong>", "Unlimited Disk Space","Unlimited Bandwidth","Unlimited Pop3 Email","Unlimited Databases", "Unlimited Addon Domains","Magic Auto Backup Restore","Domain Gratis Selamanya","Unlimited SSL Gratis Selamanya", "Private Name Server", "Prioritas Layanan Support", "&#9733;&#9733;&#9733;&#9733;&#9733;", "SpamExpert Pro Mail Protector")),
);
$priceButtonWording = "Pilih Sekarang";
$phpFeaturesLine = "Powerful dengan limit PHP yang lebih besar";
$phpFeatures1 = array("max execution time 300s","max execution time 300s","PHP memory limit 1024MB");
$phpFeatures2 = array("post max size 128MB", "upload max file size 128 MB", "max input vars 2500");
$includesAll = "Semua Paket Hosting Sudah Termasuk";
$includeList1 = array(
    array('images' => 'assets/php-semua-versi.png', 'title' => 'PHP Semua Versi', 'subtitle' => 'Pilih mulai dari versi 5.3 s/d 7. Ubah sesuka anda!'),
    array('images' => 'assets/mysql-versi-56.png', 'title' => 'PHP Semua Versi', 'subtitle' => 'Pilih mulai dari versi 5.3 s/d 7. Ubah sesuka anda!'),
    array('images' => 'assets/panel-hosting-cpanel.png', 'title' => 'PHP Semua Versi', 'subtitle' => 'Pilih mulai dari versi 5.3 s/d 7. Ubah sesuka anda!')
);
$includeList2 = array(
    array('images' => 'assets/garansi-uptime-99.png', 'title' => 'PHP Semua Versi', 'subtitle' => 'Pilih mulai dari versi 5.3 s/d 7. Ubah sesuka anda!'),
    array('images' => 'assets/database-innodb-unlimited.png', 'title' => 'PHP Semua Versi', 'subtitle' => 'Pilih mulai dari versi 5.3 s/d 7. Ubah sesuka anda!'),
    array('images' => 'assets/wildcard-remore-mysql.png', 'title' => 'PHP Semua Versi', 'subtitle' => 'Pilih mulai dari versi 5.3 s/d 7. Ubah sesuka anda!')
);
$laravelSupportTitle = "Mendukung Penuh Framework Laravel";
$laravelSupportSubtitle = "Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda";
$laravelSupportFeatures = array("Install Laravel 1 klik dengan Softaculus Installer","Mendukung ekstensi PHP MCrypt, phar, mbstring, json, dan fileinfo","Tersedia Composer dan SSH untuk menginstall package pilihan Anda");
$modulesTitle = "Module Lengkap untuk Menjalankan Aplikasi PHP Anda.";
$modulesList = array("IcePHP","apc","apcu","apm","ares","bcmath","bcompiler","big_int","bitset","bloomy","bz2_filter","clamav","coin_acceptor","crack","dba","http","huffman","idn","igbinary","imagick","imap","inclued","inotify","interbase","intl","ioncube_loader","ioncube_Ioader_4","jsmin","json","Idap","nd_pdo_mysql","oauth","oci8","Odbc","opcache","pdf","pdo","pdo_dblib","pdo_firebird","pdo_mysql","pdo_odbc","pdo_pgsql","pdo_sqlite","pgsql","phalcon","stats","stem","stomp","suhosin","sybase_ct","sysvmsg","sysvsem","sysvshm","tidy","timezonedb","trader","translit","uploadprogress","uri_template","uuid");
$supportTitle = "Linux Hosting yang Stabil dengan Teknologi LVE";
$supportSubTitle = "SuperMicro Intel Xeon 24-Cores server dengan RAM 128 GB dan teknologi LVE CloudLinux untuk stabilitas server Anda_ Dilengkapi dengan SSD untuk kecepatan MySQL dan caching, Apache load balancer berbasis LiteSpeed Technologies, CageFS security, Raid-10 protection dan auto backup untuk keamanan website PHP Anda.";
$share = "Bagikan Jika Anda Menyukai Halaman Ini";
$help = "Perlu Bantuan? Hubungi Kami: 0247-5305505";
$footerMenu1 = array(
    array("title" => "HUBUNGI KAMI",
    "list" => array("0274-5305505","Senin - Minggu","24 Jam Nonstop","&nbsp;","Jl. Selokan Mataram Monjali","Karangjati MT 1/304","Sinduadi, Mlati, Sleman","Yogyakarta 55284")
    ),
    array("title" => "LAYANAN",
    "list" => array("Domain","Shared Hosting","Cloud VPS Hosting","Managed VPS Hosting","Web Builder","Keamanan SSL / HTTPS","Jasa Pembuatan Website","Program Affiliasi")
    ),
    array("title" => "SERVICE HOSTING",
    "list" => array("Hosting Murah","Hosting Indonesia","Hosting Singapura SG","Hosting PHP","Hosting Wordpress","Hosting Laravel")
    ),
    array("title" => "TUTORIAL",
    "list" => array("Knowledgebase","Blog","Cara Pembayaran")
    ));
    $footerMenu2 = array(
        array("is_list" => true, "title" => "TENTANG KAMI",
        "list" => array("Tim Niagahoster","Karir","Events","Penawaran & Promo Spesial","Kontak Kami")
        ),
        array("is_list" => true, "title" => "KENAPA PILIH NIAGAHOSTER? ",
        "list" => array("Support Terbaik","Garansi Harga Termurah","Domain Gratis Selamanya","Datacenter Hosting Terbaik","Review Pelanggan")
        )
        ,
        array("is_list" => false, "title" => "NEWSLETTER",
        "list" => "assets/subcribe.png"
        )
        ,
        array("is_list" => false, "title" => "&nbsp;",
        "list" => "assets/socmed.png"
        )
    );
