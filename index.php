<?php
include 'data.php';
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Niagahoster</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Your page description here" />
    <meta name="author" content="" />

    <!-- css -->
    <link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="css/flexslider.css" rel="stylesheet" />
    <link href="css/prettyPhoto.css" rel="stylesheet" />
    <link href="css/camera.css" rel="stylesheet" />
    <link href="css/jquery.bxslider.css" rel="stylesheet" />
    <link href="css/style.css?v=<?php echo uniqid(); ?>" rel="stylesheet" />

    <!-- Theme skin -->
    <link href="color/default.css" rel="stylesheet" />

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png" />
    <link rel="shortcut icon" href="assets/logo.png" />
    <style>
        div.is_bestseller h4 h2 strong {
            color: white;
        }
    </style>
</head>

<body>

    <div id="wrapper">

        <!-- start header -->
        <header>
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <p class="topcontact"><img src="assets/top-badge.png" alt="" />&nbsp;<?php echo $topBadgeTitle; ?></p>
                        </div>
                        <div class="span6">

                            <ul class="social-network">
                                <li><i class="icon-phone icon-white"></i> 0274-5305505</li>
                                <li><i class="icon-comment icon-white"></i> Live Chat</li>
                                <li><i class="icon-user icon-white"></i> Member Area</li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <div class="container">


                <div class="row nomargin">
                    <div class="span3">
                        <div class="logo">
                            <a href="index.html"><img src="<?php echo $logo; ?>" alt="" /></a>
                        </div>
                    </div>
                    <div class="span9">
                        <div class="navbar navbar-static-top">
                            <div class="navigation">
                                <nav>
                                    <ul class="nav topnav">
                                        <?php
                                        foreach ($listMenu as $menu) {
                                        ?>
                                            <li class="dropdown">
                                                <a href="index.html"> <?php echo $menu; ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </nav>
                            </div>
                            <!-- end navigation -->
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

        <!-- section featured -->
        <section id="featured">
            <!-- slideshow start here -->
            <div class="camera_wrap" id="camera-slidea">
                <div data-src="img/slides/camera/slide1/img1.jpg">
                    <div class="camera_caption fadeFromLeft">
                        <div class="container">
                            <div class="row">
                                <div class="span6">
                                    <h2 class="animated fadeInDown"><strong><?php echo $mainTitle; ?></strong></h2>
                                    <p class="animated fadeInUp" style="  font-size: 35px;line-height: 1em;"><?php echo $mainDescription; ?></p>
                                    <ul style="list-style: none; margin-left:-5px">
                                        <?php
                                        foreach ($listHeaderMenu as $val) {
                                        ?>
                                            <li style="line-height: 2em;"><img src="assets/greencheck.png" style="width: 20px">&nbsp;<?php echo $val; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="span6">
                                    <img src="assets/server.png" alt="" class="animated bounceInDown delay1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- slideshow end here -->
        </section>
        <!-- /section featured -->

        <section id="content">
            <div class="container">


                <div class="row">
                    <div class="span12">
                        <div class="row">
                            <?php
                            foreach ($flyLeftMenu as $key => $leftMenu) {
                            ?>
                                <div class="span4">
                                    <div class="box flyLeft">
                                        <div class="text">
                                            <div class="pricing-box-wrap animated-fast flyIn animated fadeInUp" style="border: 0px solid #e6e6e6;">
                                                <div class="pricing-heading" style="height: 150px;background: #fff;">
                                                    <h4><img src="<?php echo $leftMenu['imageLogo']; ?>" style="max-height: 100%;max-width: 100%;width: auto;height: auto;position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;"></h4>
                                                </div>
                                                <div class="pricing-terms" style="background: #fff;border: 0px solid #e6e6e6;">
                                                    <?php echo $leftMenu['wording']; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="span12">
                        <div class="solidline"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="span12">
                        <div class="row">
                            <div class="span12">
                                <div class="aligncenter">
                                    <h3><strong><?php echo $wideTitle; ?></strong></h3>
                                    <p style="font-size: x-large"><?php echo $wideSubTitle; ?>
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <?php
                            foreach ($priceMenus as $key => $priceMenu) {
                            ?>
                                <div class="span3">
                                    <div class="pricing-box-wrap animated-fast flyIn" style="width: 120%;">
                                        <div class="pricing-heading" style="background: <?php echo $priceMenu['is_bestseller'] ? "#008fee" : "#ffffff"; ?>">
                                            <?php if ($priceMenu['is_bestseller']) { ?>
                                                <img src="assets/bestseller.png" class="ribbon" style=" position: absolute;top: -8px;left: -8px;" />
                                            <?php } ?>
                                            <h3 style=" color: black"><strong><?php echo $priceMenu['title']; ?></strong></h3>
                                        </div>
                                        <div class="pricing-terms <?php echo $priceMenu['is_bestseller'] ? "is_bestseller" : ""; ?>" style="<?php echo $priceMenu['is_bestseller'] ? "background:#008fee;border: 0px solid #e6e6e6;" : "background:#ffffff;border: 1px solid #e6e6e6;"; ?>">
                                            <?php if ($priceMenu['is_bestseller']) { ?>
                                                <h7 style=" color: white;"><s><?php echo $priceMenu['real_price']; ?></s></h7>
                                                <h4 style=" color: white;"><?php echo $priceMenu['disc_price']; ?></h4>
                                            <?php } else { ?>
                                                <h7 style=" color: black;"><s><?php echo $priceMenu['real_price']; ?></s></h7>
                                                <h4 style=" color: black;"><?php echo $priceMenu['disc_price']; ?></h4>
                                            <?php } ?>
                                        </div>
                                        <div class="pricing-terms" style="<?php echo $priceMenu['is_bestseller'] ? "background: #007fde;border: 0px solid #e6e6e6;padding: 0px 0 0px;" : "background: #ffffff;border: 1px solid #e6e6e6;padding: 0px 0 0px;"; ?>">
                                            <h6 style=" color: black;"><strong><?php echo $priceMenu['user_count']; ?></strong> Pengguna Terdaftar</h6>
                                        </div>
                                        <div class="pricing-content">
                                            <ul style="padding: 20px;">
                                                <?php
                                                foreach ($priceMenu['features'] as $feature) {
                                                ?>
                                                    <li style="border-bottom: 0px solid #e9e9e9;padding: 8px 0 0px 0;"> <?php echo $feature; ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="pricing-action" style="background: #fff;">
                                            <a href="#" class="btn btn-medium btn-theme" style="border-radius:25px;border: <?php echo $priceMenu['is_bestseller'] ? "0px" : "2px" ?> solid #000;background: <?php echo $priceMenu['is_bestseller'] ? "#008fee" : "#fff" ?>;color: black !important;"><strong><?php echo $priceButtonWording; ?></strong></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>



                        </div>


                    </div>

                    <div class="row">
                        <div class="span12">

                            <center>
                                <h3 class="title"><?php echo $phpFeaturesLine; ?></h3>
                            </center>
                            <div class="span1">

                            </div>
                            <div class="span5">
                                <div class="box flyRight animated fadeInLeftBig">
                                    <div class="text">
                                        <div class="pricing-box-wrap animated-fast flyIn animated fadeInUp" style="border: 0px solid #e6e6e6;">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php foreach ($phpFeatures1 as $phpFeature1) { ?>
                                                        <tr>
                                                            <td><img src="assets/greencheck.png"><span style="margin-left:30%"><?php echo $phpFeature1; ?></span></td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span5">
                                <div class="box flyRight animated fadeInLeftBig">
                                    <div class="text">
                                        <div class="pricing-box-wrap animated-fast flyIn animated fadeInUp" style="border: 0px solid #e6e6e6;">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <?php foreach ($phpFeatures2 as $phpFeature2) { ?>
                                                        <tr>
                                                            <td><img src="assets/greencheck.png"><span style="margin-left:30%"><?php echo $phpFeature2; ?></span></td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span1">

                            </div>

                        </div>
                    </div>

                </div>
        </section>


        <section id="features">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <center>
                            <h3><?php echo $includesAll; ?></h3>
                        </center>
                        <div class="row">
                            <div class="grid cs-style-4">
                                <?php
                                foreach ($includeList1 as $key => $include) {
                                ?>
                                    <div class="span4">
                                        <div class="item">
                                            <figure>
                                                <center>
                                                    <h4><img src="<?php echo $include['images'] ?>"><br>
                                                        <?php echo $include['title'] ?></h4>
                                                    <?php echo $include['subtitle'] ?>
                                                </center>
                                            </figure>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="grid cs-style-4">
                                <?php
                                foreach ($includeList2 as $key => $include) {
                                ?>
                                    <div class="span4">
                                        <div class="item">
                                            <figure>
                                                <center>
                                                    <h4><img src="<?php echo $include['images'] ?>"><br>
                                                        <?php echo $include['title'] ?></h4>
                                                    <?php echo $include['subtitle'] ?>
                                                </center>
                                            </figure>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="featured">
            <!-- slideshow start here -->
            <div class="camera_wrap" id="camera-slidea">
                <div data-src="img/slides/camera/slide1/img1.jpg">
                    <div class="camera_caption fadeFromLeft">
                        <div class="container">
                            <div class="row">
                                <center>
                                    <h3><?php echo $laravelSupportTitle; ?></h3>
                                </center>
                                <div class="span6">
                                    <p class="animated fadeInUp" style="font-size: 20px;line-height: 2em;"><?php echo $laravelSupportSubtitle; ?></p>
                                    <ul style="list-style: none">
                                        <?php
                                        foreach ($laravelSupportFeatures as $val) {
                                        ?>
                                            <li style="line-height: 2em;"><img src="assets/greencheck.png" style="width: 20px">&nbsp;<?php echo $val; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="span6">
                                    <img src="assets/laravel.png" alt="" class="animated bounceInUp delay1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- slideshow end here -->
        </section>

        <section id="modules" style="margin-top: 25px">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <center>
                            <h3 class="title"><?php echo $modulesTitle; ?></h3>
                        </center>
                        <div class="row">
                            <div class="grid cs-style-4">
                                <?php
                                foreach ($modulesList as $mdl) {
                                ?>
                                    <div class="span3">
                                        <?php echo $mdl; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <center><a href="#" class="btn btn-medium btn-theme" style="border-radius:25px;border: 2px solid #000;background: #fff; color: black !important;"><strong>Selengkapnya</strong></a></center>
                    </div>
                </div>
            </div>
        </section>

        <section id="support">
            <div class="camera_wrap" id="camera-slidea">
                <div data-src="img/slides/camera/slide1/img1.jpg">
                    <div class="camera_caption fadeFromLeft">
                        <div class="container">
                            <div class="row">

                                <div class="span6">
                                    <h3><?php echo $supportTitle; ?></h3>
                                    <p class="animated fadeInUp" style="font-size: 17px;line-height: 2em;"><?php echo $supportSubTitle; ?></p>
                                    <a href="#" class="btn btn-medium btn-theme" style="border-radius:25px;border: 0px solid #000;background: #0090ef; color: white !important;"><strong>Pilih Hosting Anda</strong></a>
                                </div>
                                <div class="span6">
                                    <img src="assets/image-support.png" alt="" class="animated bounceInRight delay1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="share" style="background:#f7f7f7">
            <div class="container">
                <div class="row" style="margin-top:30px;margin-bottom:0px">
                    <div class="span9">
                        <div class="widget">
                            <h5 class="widgetheading"><?php echo $share; ?></h5>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="widget">
                            <h5 class="widgetheading">
                                <center><img src="assets/social.png" alt="" class="animated bounceInRight delay1" /></center>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="help" style="background:#00a2f3">
            <div class="container">
                <div class="row">
                    <div class="span9">
                        <div class="widget">
                            <h5 class="widgetheading">
                                <h4><?php echo $help; ?></h3>
                            </h5>
                        </div>
                    </div>
                    <div class="span3">
                        <div class="widget">
                            <h5 class="widgetheading">
                                <center><img src="assets/livechat.png" alt="" class="animated bounceInRight delay1" /></center>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <footer>
            <div class="container">
                <div class="row">
                    <?php
                    foreach ($footerMenu1 as $foot) {
                    ?>
                        <div class="span3">
                            <div class="widget">
                                <h5 class="widgetheading"><?php echo $foot['title']; ?></h5>
                                <ul class="link-list">
                                    <?php
                                    foreach ($foot['list'] as $list) {
                                    ?>
                                        <li><a href="#"><?php echo $list; ?></a></li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <?php
                    foreach ($footerMenu2 as $foot) {
                    ?>
                        <div class="span3">
                            <div class="widget">
                                <h5 class="widgetheading"><?php echo $foot['title']; ?></h5>
                                <?php if ($foot['is_list']) { ?>
                                    <ul class="link-list">
                                        <?php
                                        foreach ($foot['list'] as $list) {
                                        ?>
                                            <li><a href="#"><?php echo $list; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <img src="<?php echo $foot['list']; ?>">
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="span12">
                        <h6 style="color:#8e8e8e">PEMBAYARAN</h6>
                        <img src="assets/listbank.png">
                        <p style="margin-top:25px">Aktivasi instan dengan e-Payment. Hosting dan domain langsung aktif!</p>
                    </div>
                </div>
            </div>
            <div id="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="span6">
                            <div class="copyright">
                                <p><span>Copyright &copy;2016 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta<br>Cloud VPS Murah powered by Webuzo Softaculous, Inte; SSD and cloud computing technology</span></p>
                            </div>

                        </div>

                        <div class="span6">
                            <div class="credits">
                                Syarat dan Ketentuan Berlaku | Kebijakan Privasi
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bglight icon-2x active"></i></a>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.js"></script>

    <script src="js/modernizr.custom.js"></script>
    <script src="js/toucheffects.js"></script>
    <script src="js/google-code-prettify/prettify.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/camera/camera.js"></script>
    <script src="js/camera/setting.js"></script>

    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/portfolio/jquery.quicksand.js"></script>
    <script src="js/portfolio/setting.js"></script>

    <script src="js/jquery.flexslider.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/inview.js"></script>

    <!-- Template Custom JavaScript File -->
    <script src="js/custom.js"></script>

</body>

</html>